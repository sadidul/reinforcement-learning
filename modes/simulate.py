from modes.mode import Mode


class Simulate(Mode):
    def __init__(self):
        super(Simulate, self).__init__()
        print("Simulating...")
