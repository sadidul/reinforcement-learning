from gym import Env


class Environment(Env):
    def step(self, action):
        pass

    def reset(self):
        pass

    def render(self, mode='human'):
        pass

    def __init__(self):
        pass
