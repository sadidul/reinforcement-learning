from modes.mode import Mode
from modes.simulate import Simulate


class Main(Mode):
    def __init__(self):
        super(Main, self).__init__()

        if self._mode == "simulate":
            Simulate()


if __name__ == '__main__':
    Main()