from configparser import ConfigParser
from os.path import join
import os
import sys


class Config(ConfigParser):
    """
    A class used to represent configuration for the project. This class reads config.{environment}.ini
    file

    Attributes
    ----------
    environment : str
        It is the environment for the config file, like: local, remote, production

    project_dir : str
        Path of the project

    __config_location : str
        Path for config file

    Methods
    -------
    save()
        saves the config file with updated values or added attributes
    """
    def __init__(self, project_dir=None):
        super().__init__(comment_prefixes='/', allow_no_value=True)
        self.environment = "local"
        if project_dir:
            self.project_dir = join("/", *project_dir.split("/"))
        else:
            self.project_dir = os.path.dirname(sys.modules['__main__'].__file__)

        self.__config_location = join(self.project_dir, "config.{}.ini".format(self.environment.lower()))
        self.read(self.__config_location)

    def save(self):
        """
        Saves the config file with updated value or added attributes
        """
        with open(self.__config_location, "w") as file:
            self.write(file)
